<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
* Checks the current page and returns true or false if it should have a masthead or not.
*
* @return bool	Whether the current page should have a masthead or not.
*/
function has_masthead() {
	if ( is_singular( array('post-type-example1', 'post-type-example1') ) ) {
		// Conditions for NOT having masthead
		return false;
	} else {
		return true;
	}
}

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
	if (!in_array(basename(get_permalink()), $classes)) {
	  $classes[] = basename(get_permalink());
	}
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
	$classes[] = 'sidebar-primary';
  }

  // Add class if no masthead is showing
  if ( !has_masthead() ) {
  	$classes[] = 'no-masthead';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'visceral') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * ALLOW SVG UPLOADS
 */
function theme_allow_svg_uploads($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\theme_allow_svg_uploads');
// END ALLOW SVG UPLOADS


/**
* Remove hentry from post_class
*/
function remove_hentry_class( $classes ) {
	$classes = array_diff( $classes, array( 'hentry' ) );
	return $classes;
}
add_filter( 'post_class', __NAMESPACE__ . '\\remove_hentry_class' );



/** 
* Includes Media items in search results when including/editing links in editor
* Source: https://core.trac.wordpress.org/ticket/22180
*/

add_filter( 'wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args' );

// Query for all post statuses so attachments are returned
function visceral_modify_link_query_args( $query ) {
	// Attachment post types have status of inherit. This allows them to be included.
	$query['post_status'] = array('publish', 'inherit');
	return $query;
}

add_filter( 'wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results' );

// Link to media file URL instead of attachment page
function visceral_modify_link_query_results( $results, $query ) {
  foreach ( $results as &$result ) {
	if ( 'Media' === $result['info'] ) {
	  $result['permalink'] = wp_get_attachment_url( $result['ID'] );
	}
  }
  return $results;
}

/** 
* Customize login screen
* 
*/

function visceral_custom_login_style() {
  // Set login color; default is WordPress blue
  $login_color = '#0085ba';
  if ( get_theme_mod( 'login_color' ) )  {
    $login_color = get_theme_mod( 'login_color' );
  } 

  // Set button style only if a custom color is chosen
   if ( get_theme_mod( 'login_color' ) )  {
    $login_button_style = '.visc-login-inner input.button {
      width: 100%;
      height: inherit !important;
      padding: 16px 22px !important;
      margin-bottom: 20px;
      border-radius: 0;
      font-size: 12px;
      line-height: 18px !important;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-weight: bold;
      background: ' . $login_color . ' !important;
      border-color: ' . $login_color . ' !important;
      box-shadow: none !important;
      color: #fff;
      text-decoration: none !important;
      text-shadow: none !important;
      transition: all 0.3s;
    }

    .visc-login-inner input.button:hover, .visc-login-inner input.button:focus {
	  background: ' . $login_color . ' !important;
	  border-color: ' . $login_color . ' !important;
	  opacity: 0.8;
  	}';
  }  
  $style_output = '<style type="text/css">';
  // Change login logo
  if ( get_theme_mod( 'login_logo' ) )  {
    list($width,$height) = getimagesize(get_theme_mod( "login_logo" ));
    
    $style_output .= 'h1 a {
        background-image:url('. get_theme_mod( "login_logo" ) .') !important;
        background-size: '. $width .'px !important;
        width: '. $width .'px !important;
        height: '. $height .'px !important;
      }';
  }
  // Change login background image
  if ( get_theme_mod( 'login_bg_image' ) )  {    
    $style_output .= 'body {
        background-image:url('. get_theme_mod( "login_bg_image" ) .');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
      }';
  }

  $style_output .= '#login {
    width: 425px;
  }

  .visc-login-inner {
    background: rgba(255, 255, 255, .8);
    padding: 50px;
  }

  .login label {
    color: black;
  }

  .visc-login-inner form {
    margin-top: 40px;
    padding: 0;
    background: none;
    box-shadow: none;
  }

  .visc-login-inner form label span {
    clip: rect(1px, 1px, 1px, 1px);
    position: absolute !important;
    height: 1px;
    width: 1px;
    overflow: hidden;
  }

  .visc-login-inner form input[type="text"], .visc-login-inner form input[type="password"] {
    border: 1px solid #d5d5d6 !important;
    border-radius: 1px !important;
    background: rgba(255, 255, 255, .4);;
    box-shadow: none !important;
    padding: 17px !important;
    margin-bottom: 0 !important;
    font-size: 16px;
    color: #000000;
    letter-spacing: 0;
  }

  .visc-login-inner form input[type="text"]:focus, .visc-login-inner form input[type="password"]:focus {
    border-color: ' . $login_color . ' !important;
    outline: none;
    box-shadow: none;
  }

  .visc-login-inner form input#user_pass {
    margin-bottom: 17px !important;
  }

  ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: black;
  }
  ::-moz-placeholder { /* Firefox 19+ */
    color: black;
  }
  :-ms-input-placeholder { /* IE 10+ */
    color: black;
  }
  :-moz-placeholder { /* Firefox 18- */
    color: black;
  }

  input[type=checkbox]:checked:before {
    color: black !important;
  }

  input[type=checkbox]:focus {
    box-shadow: none !important;
    border-color: ' . $login_color . ' !important;
  }

  '
  . $login_button_style .
  '

  .login form .forgetmenot {
    margin-top: 0 !important;
    margin-bottom: 17px !important;
  }

  .login #backtoblog, .login #nav, #visceral-brand {
    text-align: center;
    font-size: 16px;
  }

  .login #backtoblog {
    margin-top: 10px !important;
  }

  .login #backtoblog a, .login #nav a, #login_error a {
    color: ' . $login_color . ';
  }

  .login #backtoblog a:hover, .login #nav a:hover, #login_error a:hover, .login #backtoblog a:focus, .login #nav a:focus, #login_error a:focus  {
    color: ' . $login_color . ';
    opacity: 0.8;
  }

  #visceral-brand {
    margin-top: 50px;
    color: black;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  #visceral-brand a {
    text-decoration: none;
    color: ' . $login_color . ';
  }

  #visceral-brand a:hover, #visceral-brand a:focus {
  	color: ' . $login_color . ';
    opacity: 0.8;
  }

  #visceral-brand img {
    transition: all 0.3s;
  }

  #visceral-brand img:hover, #visceral-brand img:focus {
    opacity: 0.8;
  }
  
  #visceral-brand a#brand-img {
  	background-image: url(' . get_template_directory_uri() . '/dist/images/visceral-logo.svg);
  	background-repeat: no-repeat;
  	width: 50px;
  	height: 34px;
    margin-right: 18px; 
    transition: all 0.3s; 
  }

  #lostpasswordform #wp-submit {
    margin-top: 17px;
  }

  .login .message, #login_error {
    background: rgba(117, 152, 188, .3) !important;
    border: 0 !important;
    box-shadow: none !important;
  }

  #login_error {
    background: rgba(200, 16, 46, .3) !important;
  }

  .password-input-wrapper {
    width: 100% !important;
    margin-bottom: 17px !important;
  }

  .wp-hide-pw {
    text-align: center !important;
  }

  .dark-theme .visc-login-inner {
	background: rgba(0, 0, 0, .8);
  }

  .login .dark-theme label {
    color: #fff !important;
  }

   .login .dark-theme #backtoblog a, .login .dark-theme #nav a, .dark-theme #login_error a {
    color: #fff;
  }

   .login .dark-theme #backtoblog a:hover, .login .dark-theme #nav a:hover, .dark-theme #login_error a:hover, .login .dark-theme #backtoblog a:focus, .login .dark-theme #nav a:focus, .dark-theme #login_error a:focus  {
    color: #fff;
    opacity: 0.8;
  }

  .dark-theme #visceral-brand {
    margin-top: 50px;
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .dark-theme #visceral-brand a {
    text-decoration: none;
    color: #fff;
  }

  .dark-theme #visceral-brand a:hover, #visceral-brand a:focus {
  	color: #fff;
    opacity: 0.8;
  }

  .dark-theme #visceral-brand a#brand-img {
  	background-image: url(' . get_template_directory_uri() . '/dist/images/visceral-logo-white.svg);
  }
  ';

  $style_output .= '</style>';

  echo $style_output;
}
add_action('login_head', __NAMESPACE__ . '\\visceral_custom_login_style');

// Adds javascript to run on login page
function visc_login_scripts() {
    wp_enqueue_script( 'visc-login', get_template_directory_uri() . '/dist/scripts/login.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\visc_login_scripts' );

// Change login theme
if ( get_theme_mod( 'login_form_theme' ) )  {
	function visceral_custom_login_script() {
	  $script_output = '<script type="text/javascript">';
	  $script_output .= "jQuery(document).ready(function($){
	    $(\"#login\").addClass(\"dark-theme\");
	  });";
	  $script_output .= '</script>';
	  echo $script_output;
	}

	add_action('login_head', __NAMESPACE__ . '\\visceral_custom_login_script');  
}

// Change login logo
function visceral_login_logo_url() {
  if ( get_theme_mod( 'login_logo' ) )  {
    return get_bloginfo( 'url' );
  }  
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\visceral_login_logo_url' );

// Change login logo title
function visceral_login_logo_url_title() {
  if ( get_theme_mod( 'login_logo' ) )  {
    return get_bloginfo( 'name' );
  }  
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\visceral_login_logo_url_title' );


/**
 * CUSTOM FONT FORMATS
 */
function theme_tiny_mce_formats_button( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');
// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Split Divider',
			'selector' => '*',
			'classes' => 'split-divider'
		),
		array(
			'title' => 'Button',
			'selector' => 'a',
			'classes' => 'btn button'
		),
		array(
			'title' => 'Number Ticker',
			'inline' => 'span',
			'classes' => 'ticker'
		)
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );
	return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// END CUSTOM FONT FORMATS



/**
 * FILTER SEARCH BY POST TYPE
 */
function filter_search_by_post_type($post_types) {
	if ( !empty( $_GET['filter'] ) ) {
		$post_types = array( sanitize_text_field($_GET['filter']) );
	}
	return $post_types;
}
add_filter('searchwp_indexed_post_types', __NAMESPACE__ . '\\filter_search_by_post_type');
function customize_search_results_query( $query ) {
	if ( $query->is_search() && $query->is_main_query() && !empty( $_GET['filter'] ) ) {
		$query->query_vars['post_type'] = sanitize_text_field( $_GET['filter'] ); // Exclude my featured category because I display that elsewhere
	}
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\\customize_search_results_query' );
// END FILTER SEARCH BY POST TYPE


/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast( $html ) {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast' );
// END LOWER YOAST META BOX

/* END HOOKS */

/* SHORTCODES */

/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function visceral_query_shortcode($atts) {
	extract(shortcode_atts(array(
		'container_class' => '',
		'template' => 'templates/list-item',
		'show_pagination' => 0,
		'no_posts' => '',
		'post_type' => 'post',
		'posts_per_page' => 6,
		'offset' => 0,
		'order' => 'DESC',
		'orderby' => 'post_date',
		'post__not_in' => '',
		'taxonomy' => '',
		'terms' => ''
	), $atts));

	$markup = '';
	
	// Return either a single post type or an array of post types depending on how many were passed in.
	// Note: We need to do this because the Intuitive Custom Post Type plugin won't work with an array of a single post type.
	$post_type = (strpos($post_type, ',') > 0) ? explode(', ', $post_type) : $post_type;
	
	$args = array(
		'post_type' => $post_type,
		'posts_per_page' => $posts_per_page,
		'offset' => $offset,
		'order' => $order,
		'orderby' => $orderby,
		'post__not_in' => explode(', ', $post__not_in),
	);
	if ( $taxonomy && $terms ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => explode(', ', $terms),
			),
		);
	}
	$query = new \WP_Query($args);

	if ( $query->have_posts() ) :
		$markup .= '<div class="' . $container_class . '">';
		while ($query->have_posts()) : $query->the_post();
			ob_start();
			get_template_part($template, get_post_type());
			$markup .= ob_get_contents();
			ob_end_clean();
		endwhile;
		$markup .= '</div>';
		wp_reset_postdata();

		// Pagination
		$total = $query->max_num_pages;
		// only bother with pagination if we have more than 1 page
		if ( $show_pagination && $total > 1 ) :
			$big = 999999999; // need an unlikely integer
			$current_page = max( 1, get_query_var('paged') );
			$pagination = paginate_links( array(
				'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'    => '?paged=%#%',
				'current'   => $current_page,
				'total'     => $total,
				'type'      => 'plain',
				'prev_next' => true,
				'prev_text' => '<span class="icon-angle-left">' . __('Previous', 'visceral') . '</span>',
				'next_text' => '<span class="icon-angle-right">' . __('Next', 'visceral') . '</span>'
			) );

			$markup .= '<nav class="pagination text-center">';
				$markup .= $pagination;
			$markup .= '</nav>';
		endif;
	else :
		$markup = $no_posts;
	endif;

	return $markup;
}
add_shortcode( 'visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode' );

/* END SHORTCODES */

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 *
 * @return array 							 Returns an of related posts
 */
function visceral_related_posts($posts_per_page = 3, $post_type = 'post', $related_posts_field_name = 'related_posts') {
	// Set up the an array to hold them
	$related_posts = array();
	// Get post ID's from related field
	$selected_posts_ids = get_post_meta( null, $related_posts_field_name, true );

	// Add each post object to our array
	// Subtract each post from total posts
	if ( $selected_posts_ids ) {
		$args = array(
			'include' => $selected_posts_ids,
		);
		$selected_posts = get_posts( $args );
		foreach ( $selected_posts as $post_object ) {
			array_push($related_posts, $post_object );
			$posts_per_page --;
		}
	}

	// If we need more posts, let's get some from recent posts
	if ( $posts_per_page ) {
		// Don't repeat the posts that we already have
		$exclude_posts = $selected_posts_ids ? $selected_posts_ids : []; // Make sure it's an array
		array_push($exclude_posts, get_the_id());	

		$args = array(
			'post_type' => $post_type,
			'posts_per_page' => $posts_per_page,
			'exclude' => $exclude_posts,
		);
		$even_more_posts = get_posts( $args );
		foreach ( $even_more_posts as $post_object ) {
			array_push( $related_posts, $post_object );
		}
	}

	return $related_posts;
}
// END RELATED POSTS FUNCTION


/**
* Takes a string and returns a truncated version. Also strips out shortcodes
*
* @param  string  $text   String to truncate
* @param  integer $length Character count limit for truncation
* @param  string  $append Appended to the end of the string if character count exceeds limit
* @return string          Truncated string
*/
function truncate_text( $text='', $length = 150, $append = '...') {
	$new_text = preg_replace(" ([.*?])",'',$text);
	$new_text = strip_shortcodes($new_text);
	$new_text = strip_tags($new_text);
	$new_text = substr($new_text, 0, $length);
	if ( strlen($new_text) == $length ) {
		$new_text = substr($new_text, 0, strripos($new_text, " "));
		$new_text = $new_text . $append;
	}

	return $new_text;
}

/**
* Returns an image with a class for fitting to a certain aspect ratio
*
* @param  integer $aspect_ratio_width   Aspect Ratio Width
* @param  integer $aspect_ratio_height   Aspect Ratio Height
* @param  string  $image_size   Size of featured image to return
* @return string  markup of image
*/
function get_aspect_ratio_image($aspect_ratio_width = 1, $aspect_ratio_height = 1, $image_size = 'medium') {
    $image = wp_get_attachment_image_src(get_post_thumbnail_id(), $image_size);
    $w     = $image[1];
    $h     = $image[2];
    $class = ( ($h / $w) > ($aspect_ratio_height / $aspect_ratio_width) ) ? 'portrait' : '';
    return get_the_post_thumbnail(get_the_id(), $image_size, array('class' => $class));
}

/* END GENERAL PURPOSE FUNCTIONS */