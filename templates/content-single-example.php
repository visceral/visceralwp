<?php
/*
 * Example custom post type markup
 *
 * This example includes Schema.org microdata for people
 * For more details on the Person Schema, see https://schema.org/Person
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<article itemscope itemtype="http://schema.org/Person" class="bio">

		<?php // Use the <meta> tag to include visibly hidden schema data ?>
		<meta itemprop="name" content="<?php the_title(); ?>" />

		<?php // Check for post meta
		$job_title = get_post_meta(get_the_id(), 'job_title' )[0];
		if ( !empty( $job_title ) ) :  ?>
			<p itemprop="jobtitle"><?php echo $job_title; ?></p>
		<?php endif; ?>

		<?php // Example of .img-cover usage ?>
		<div class="img-cover">
			<?php if ( has_post_thumbnail() ) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
				$w     = $image[1];
				$h     = $image[2];
				$class = ( $w < $h ) ? 'portrait' : '';
				the_post_thumbnail('medium', array('class' => $class, 'itemprop' => 'image') );
			} ?>
		</div>

		<div itemprop="description">
			<?php the_content(); ?>
		</div>
	</article>
<?php endwhile; ?>
