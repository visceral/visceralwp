<footer class="site-footer content-info">
	<div class="container-fluid">
		<?php dynamic_sidebar('sidebar-footer'); ?>
		<nav>
			<?php
			if (has_nav_menu('footer_menu')) :
				wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'nav list-inline']);
			endif;
			?>
		</nav>
		<p class="copyright">&copy; <?php echo date('Y'); ?> <?php echo get_theme_mod( 'copyright_textbox', get_bloginfo('name') ); ?></p>
	</div>
</footer>
