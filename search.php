<?php 
// Set up results numbers
global $wp_query;
$total = $wp_query->found_posts;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$post_per_page = $wp_query->query_vars['posts_per_page'];
$offset = ( $paged - 1 ) * $post_per_page;
$begin = $offset + 1;
$end = ( $paged*$post_per_page < $total ) ? $paged * $post_per_page : $total;

// Save the original query
if ( isset( $_GET['s'] ) ) {
	$original_search = sanitize_text_field( $_GET['s'] );
}

// Create content for filter dropdown
$args = array(
	'public' => true,
);
$post_types = get_post_types( $args );
$filter_items = '';
if ( $post_types ) {
	$filter_items = '<option value="">' . __('Filter', 'visceral') . '</option>';
	foreach ( $post_types as $slug ) {
		if ( $slug != 'attachment' ) {
			$label = get_post_type_object( $slug )->labels->name;
			// $filter_items .= '<option><a href="' . site_url() . '/?s=' . $original_search . '&filter=' . $slug . '">' . $label . '</a></option>';
			$filter_selected = (isset($_GET['filter']) &&  sanitize_text_field( $_GET['filter'] ) == $slug ) ? 'selected' : '';
			$filter_items .= '<option name="content_filter" value="' . $slug . '"' . $filter_selected . '>' . $label . '</option>';
		}
	}
	// $filter_items .= '</ul>';
} else {
	$filter_items .= '<option>' . __('No filters', 'visceral') . '</option>';
}?>
<div class="search-results-top">
	<form action="<?php echo site_url(); ?>" class="row md-reverse row-eq-height">
		<div class="column sm-100">
			<label><span class="screen-reader-text"><?php _e('Search', 'sage'); ?></span>
				<input type="text" name="s" value="<?php echo $original_search ?>" class="js-show">
			</label>
		</div>
		
		<div class="column sm-33">
			<label><span class="screen-reader-text"><?php _e('Filter By:', 'sage'); ?></span>
				<select name="filter" onchange="this.form.submit()"><?php echo $filter_items; ?></select>
			</label>
		</div>
		<div class="js-show column sm-33">
			<input type="submit" value="<?php _e('Submit', 'sage'); ?>">
		</div>
		<div class="column sm-67">
			<h3 ><?php echo $begin . '-' . $end . ' ' . __('of', 'visceral') . ' ' . $total; ?></h3>
		</div>
	</form>
</div>
<?php if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
		<?php get_template_part('templates/content', 'search'); ?>
	<?php endwhile; ?>
	<?php $total = $wp_query->max_num_pages;
	// only bother with the rest if we have more than 1 page
	if ( $total > 1 ) : ?>
		<nav class="text-center pagination">
			<?php // get the current page
			if ( !$current_page = get_query_var('paged') )
				$current_page = 1;
			$big = 999999999; // need an unlikely integer

			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $total,
				'type' => 'plain',
				'prev_next' => true,
				'prev_text' => __('Next', 'visceral'),
				'next_text' => __('Previous', 'visceral')
			) ); ?>
		</nav>
	<?php endif; ?>	
<?php else : ?>
	<div class="alert alert-warning">
		<p><?php _e('Sorry, no results were found.', 'visceral'); ?></p>
	</div>
<?php endif; ?>
