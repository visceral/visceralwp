<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
    $wp_customize->get_setting('blogname')->transport = 'postMessage';

    // Main Sections
    $wp_customize->add_section(
        'theme_general',
        array(
            'title' => __('General Options', 'visceral'),
            'priority' => 90,
        )
    );
    $wp_customize->add_section(
        'theme_social_media',
        array(
            'title' => __('Social Media', 'visceral'),
            'priority' => 90,
        )
    );

    $wp_customize->add_section(
        'theme_login',
        array(
            'title' => __('Login', 'visceral'),
            'priority' => 90,
        )
    );

    $wp_customize->add_setting(
        'facebook_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'facebook_url',
        array(
            'label' => __('Facebook URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'twitter_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'twitter_url',
        array(
            'label' => __('Twitter URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'linkedin_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'linkedin_url',
        array(
            'label' => __('LinkedIn URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'youtube_url',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'youtube_url',
        array(
            'label' => __('YouTube URL', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );
    
    $wp_customize->add_setting(
        'copyright_textbox',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'copyright_textbox',
        array(
            'label' => __('Copyright text', 'visceral'),
            'section' => 'theme_social_media',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting(
        'login_logo',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        new \WP_Customize_Image_Control(
           $wp_customize,
           'login_logo',
           array(
               'label'      => __( 'Upload a login logo', 'visceral' ),
               'section'    => 'theme_login',
               'settings'   => 'login_logo',
               'context'    => 'your_setting_context' 
           )
       )
    );

    $wp_customize->add_setting(
        'login_bg_image',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        new \WP_Customize_Image_Control(
           $wp_customize,
           'login_bg_image',
           array(
               'label'      => __( 'Upload a login background', 'visceral' ),
               'section'    => 'theme_login',
               'settings'   => 'login_bg_image',
               'description'    => __( 'Set the background image of the login screen. Use an image that is at least 1200px X 1200px.', 'visceral' ) 
           )
       )
    );

    $wp_customize->add_setting(
        'login_color',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'login_color',
        array(
            'label' => __('Login Link and Button Color', 'visceral'),
            'section' => 'theme_login',
            'type' => 'text',
            'description'    => __( 'Select a color or enter a hex value. Ex: #FFFFFF .', 'visceral' ) 
        )
    );
    $wp_customize->add_control( 
        new \WP_Customize_Color_Control( 
        $wp_customize, 
        'login_color', 
        array(
            'label'      => __('Login Link and Button Color', 'visceral'),
            'section'    => 'theme_login',
            'settings'   => 'login_color',
            'description'    => __( 'Enter a hex or rgba value. Ex: #FFFFFF or rgba(255,255,255,1).', 'visceral' ) 
        ) ) 
    );

    $wp_customize->add_setting(
        'login_form_theme',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(
        'login_form_theme',
        array(
            'label' => __('Login Theme', 'visceral'),
            'section' => 'theme_login',
            'type' => 'radio',
            'choices'=> array('Light' => 'light-theme', 'Dark' => 'dark-theme'),
            'description'    => __( 'Choose a light or dark theme.', 'visceral' ) 
        )
    );

}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');

