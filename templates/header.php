<header class="header container-fluid">

	<div class="header__main" id="header-main">

		<a class="header__brand" href="<?= esc_url(home_url('/')); ?>">
			<?php bloginfo('name'); ?> <!-- replace with logo -->
		</a>

		<?php if (has_nav_menu('primary_menu')) : ?>
		<nav class="header__navigation">
			<?php wp_nav_menu(['theme_location' => 'primary_menu', 'menu_class' => 'nav list-inline']); ?>
		</nav>
		<?php endif; ?>

		<label class="header__mobile-icon no-print" id="mobile-nav-icon" for="nav-toggle" tabindex="0">
			<span class="screen-reader-text"><?php _e('Navigation Toggle', 'visceral'); ?></span>
			<div>
				<div class="header__menu-line"></div>
				<div class="header__menu-line"></div>
				<div class="header__menu-line"></div>
				<div class="header__menu-line"></div>
			</div>	
		</label>

		<a class="header__search-button icon-search" id="search-button" href="<?php echo site_url(); ?>/?s=" title="<?php _e('Search', 'sage');?>" aria-label="Search"><?php _e('Search', 'visceral'); ?></a>

	</div>

	<div class="header__search" id="header-search">
		<div class="flex-wrapper">
			<form role="search" method="get" action="<?php echo site_url() ?>">
				<label><span class="screen-reader-text"><?php _e('Search', 'sage'); ?></span>
					<input type="text" placeholder="<?php _e('Type here and hit enter', 'sage'); ?>..." name="s" autocomplete="off" spellcheck="false" autofocus>
				</label>
				<input type="submit" class="screen-reader-text" value="<?php _e('Submit', 'sage'); ?>">
			</form>
			<i id="search-close" class="icon-cancel" aria-label="Close"><?php _e('Close', 'visceral'); ?></i>
		</div>
	</div>
</header>

<input type="checkbox" id="nav-toggle">

<?php // Responsive-nav ?>
<?php if (has_nav_menu('mobile_menu')) :
	wp_nav_menu(['theme_location' => 'mobile_menu', 'container_id' => 'mobile-nav', 'container_class' => 'mobile-nav', 'menu_class' => 'mobile-nav__menu']);
elseif (has_nav_menu('primary_menu')) :
	wp_nav_menu(['theme_location' => 'primary_menu', 'container_id' => 'mobile-nav', 'container_class' => 'mobile-nav', 'menu_class' => 'mobile-nav__menu']);
endif;
?>